package com.example.android.project.testgorastudio.domen.usecase;

import com.example.android.project.testgorastudio.domen.exception.FailureOrLoading;
import com.example.android.project.testgorastudio.domen.model.Either;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0007\b&\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u0002*\u0006\b\u0001\u0010\u0003 \u00002\u00020\u0002:\u0001\bB\u0005\u00a2\u0006\u0002\u0010\u0004J\u0019\u0010\u0005\u001a\u00028\u00002\u0006\u0010\u0006\u001a\u00028\u0001H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0007\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\t"}, d2 = {"Lcom/example/android/project/testgorastudio/domen/usecase/UseCase;", "Type", "", "Params", "()V", "run", "params", "(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "None", "app_debug"})
public abstract class UseCase<Type extends java.lang.Object, Params extends java.lang.Object> {
    
    public UseCase() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object run(Params params, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super Type> p1);
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/example/android/project/testgorastudio/domen/usecase/UseCase$None;", "", "()V", "app_debug"})
    public static final class None {
        
        public None() {
            super();
        }
    }
}