package com.example.android.project.testgorastudio.data.repository.photo;

import com.example.android.project.testgorastudio.data.repository.mappers.PhotoDTOtoPhoto;
import com.example.android.project.testgorastudio.data.repository.photo.datasource.PhotoNetworkDataSource;
import com.example.android.project.testgorastudio.domen.model.Photo;
import com.example.android.project.testgorastudio.domen.repository.PhotoRepository;
import kotlinx.coroutines.flow.Flow;
import kotlinx.coroutines.flow.StateFlow;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b2\u0006\u0010\u000b\u001a\u00020\fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ\u0019\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000b\u001a\u00020\fH\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0010"}, d2 = {"Lcom/example/android/project/testgorastudio/data/repository/photo/PhotoRepositoryImpl;", "Lcom/example/android/project/testgorastudio/domen/repository/PhotoRepository;", "photoDao", "Lcom/example/android/project/testgorastudio/data/repository/photo/PhotoDao;", "dataSource", "Lcom/example/android/project/testgorastudio/data/repository/photo/datasource/PhotoNetworkDataSource;", "(Lcom/example/android/project/testgorastudio/data/repository/photo/PhotoDao;Lcom/example/android/project/testgorastudio/data/repository/photo/datasource/PhotoNetworkDataSource;)V", "getPhotos", "Lkotlinx/coroutines/flow/Flow;", "", "Lcom/example/android/project/testgorastudio/domen/model/Photo;", "userId", "", "(ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "loadPhotosWhenEmpty", "", "app_debug"})
public final class PhotoRepositoryImpl implements com.example.android.project.testgorastudio.domen.repository.PhotoRepository {
    private final com.example.android.project.testgorastudio.data.repository.photo.PhotoDao photoDao = null;
    private final com.example.android.project.testgorastudio.data.repository.photo.datasource.PhotoNetworkDataSource dataSource = null;
    
    @javax.inject.Inject()
    public PhotoRepositoryImpl(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.data.repository.photo.PhotoDao photoDao, @org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.data.repository.photo.datasource.PhotoNetworkDataSource dataSource) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getPhotos(int userId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlinx.coroutines.flow.Flow<? extends java.util.List<com.example.android.project.testgorastudio.domen.model.Photo>>> p1) {
        return null;
    }
    
    private final java.lang.Object loadPhotosWhenEmpty(int userId, kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
}