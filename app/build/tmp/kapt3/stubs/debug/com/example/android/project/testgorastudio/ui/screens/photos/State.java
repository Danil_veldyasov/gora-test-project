package com.example.android.project.testgorastudio.ui.screens.photos;

import com.example.android.project.testgorastudio.domen.model.Photo;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\b\u00a8\u0006\t"}, d2 = {"Lcom/example/android/project/testgorastudio/ui/screens/photos/State;", "", "()V", "Error", "Loading", "PhotosLoaded", "Lcom/example/android/project/testgorastudio/ui/screens/photos/State$Loading;", "Lcom/example/android/project/testgorastudio/ui/screens/photos/State$Error;", "Lcom/example/android/project/testgorastudio/ui/screens/photos/State$PhotosLoaded;", "app_debug"})
public abstract class State {
    
    private State() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/example/android/project/testgorastudio/ui/screens/photos/State$Loading;", "Lcom/example/android/project/testgorastudio/ui/screens/photos/State;", "()V", "app_debug"})
    public static final class Loading extends com.example.android.project.testgorastudio.ui.screens.photos.State {
        @org.jetbrains.annotations.NotNull()
        public static final com.example.android.project.testgorastudio.ui.screens.photos.State.Loading INSTANCE = null;
        
        private Loading() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/example/android/project/testgorastudio/ui/screens/photos/State$Error;", "Lcom/example/android/project/testgorastudio/ui/screens/photos/State;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "app_debug"})
    public static final class Error extends com.example.android.project.testgorastudio.ui.screens.photos.State {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String message = null;
        
        public Error(@org.jetbrains.annotations.NotNull()
        java.lang.String message) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\b"}, d2 = {"Lcom/example/android/project/testgorastudio/ui/screens/photos/State$PhotosLoaded;", "Lcom/example/android/project/testgorastudio/ui/screens/photos/State;", "listPhotos", "", "Lcom/example/android/project/testgorastudio/domen/model/Photo;", "(Ljava/util/List;)V", "getListPhotos", "()Ljava/util/List;", "app_debug"})
    public static final class PhotosLoaded extends com.example.android.project.testgorastudio.ui.screens.photos.State {
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<com.example.android.project.testgorastudio.domen.model.Photo> listPhotos = null;
        
        public PhotosLoaded(@org.jetbrains.annotations.NotNull()
        java.util.List<com.example.android.project.testgorastudio.domen.model.Photo> listPhotos) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<com.example.android.project.testgorastudio.domen.model.Photo> getListPhotos() {
            return null;
        }
    }
}