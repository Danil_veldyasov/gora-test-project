package com.example.android.project.testgorastudio.ui.screens.users;

import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;
import com.example.android.project.testgorastudio.domen.exception.FailureOrLoading;
import com.example.android.project.testgorastudio.domen.interactors.GetUsers;
import com.example.android.project.testgorastudio.domen.model.User;
import com.example.android.project.testgorastudio.domen.usecase.UseCase;
import dagger.assisted.AssistedInject;
import dagger.hilt.android.lifecycle.HiltViewModel;
import kotlinx.coroutines.Dispatchers;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\t0\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u000f"}, d2 = {"Lcom/example/android/project/testgorastudio/ui/screens/users/UsersViewModel;", "Landroidx/lifecycle/ViewModel;", "getUsers", "Lcom/example/android/project/testgorastudio/domen/interactors/GetUsers;", "savedStateHandle", "Landroidx/lifecycle/SavedStateHandle;", "(Lcom/example/android/project/testgorastudio/domen/interactors/GetUsers;Landroidx/lifecycle/SavedStateHandle;)V", "_uiState", "Lkotlinx/coroutines/flow/MutableStateFlow;", "Lcom/example/android/project/testgorastudio/ui/screens/users/State;", "initialState", "uiState", "Lkotlinx/coroutines/flow/StateFlow;", "getUiState", "()Lkotlinx/coroutines/flow/StateFlow;", "app_debug"})
public final class UsersViewModel extends androidx.lifecycle.ViewModel {
    private final com.example.android.project.testgorastudio.domen.interactors.GetUsers getUsers = null;
    private final androidx.lifecycle.SavedStateHandle savedStateHandle = null;
    private final com.example.android.project.testgorastudio.ui.screens.users.State initialState = null;
    private final kotlinx.coroutines.flow.MutableStateFlow<com.example.android.project.testgorastudio.ui.screens.users.State> _uiState = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.StateFlow<com.example.android.project.testgorastudio.ui.screens.users.State> uiState = null;
    
    @javax.inject.Inject()
    public UsersViewModel(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.domen.interactors.GetUsers getUsers, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.SavedStateHandle savedStateHandle) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.StateFlow<com.example.android.project.testgorastudio.ui.screens.users.State> getUiState() {
        return null;
    }
}