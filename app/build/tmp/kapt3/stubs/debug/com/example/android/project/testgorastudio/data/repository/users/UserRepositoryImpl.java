package com.example.android.project.testgorastudio.data.repository.users;

import com.example.android.project.testgorastudio.data.repository.mappers.UserDTOtoUser;
import com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSource;
import com.example.android.project.testgorastudio.domen.model.User;
import com.example.android.project.testgorastudio.domen.repository.UserRepository;
import kotlinx.coroutines.flow.*;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001d\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bJ\u0011\u0010\f\u001a\u00020\rH\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u000e"}, d2 = {"Lcom/example/android/project/testgorastudio/data/repository/users/UserRepositoryImpl;", "Lcom/example/android/project/testgorastudio/domen/repository/UserRepository;", "userDao", "Lcom/example/android/project/testgorastudio/data/repository/users/UserDao;", "dataSource", "Lcom/example/android/project/testgorastudio/data/repository/users/datasource/UserNetworkDataSource;", "(Lcom/example/android/project/testgorastudio/data/repository/users/UserDao;Lcom/example/android/project/testgorastudio/data/repository/users/datasource/UserNetworkDataSource;)V", "getUsers", "Lkotlinx/coroutines/flow/Flow;", "", "Lcom/example/android/project/testgorastudio/domen/model/User;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "loadUsersWhenEmpty", "", "app_debug"})
public final class UserRepositoryImpl implements com.example.android.project.testgorastudio.domen.repository.UserRepository {
    private final com.example.android.project.testgorastudio.data.repository.users.UserDao userDao = null;
    private final com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSource dataSource = null;
    
    @javax.inject.Inject()
    public UserRepositoryImpl(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.data.repository.users.UserDao userDao, @org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSource dataSource) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getUsers(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlinx.coroutines.flow.Flow<? extends java.util.List<com.example.android.project.testgorastudio.domen.model.User>>> p0) {
        return null;
    }
    
    private final java.lang.Object loadUsersWhenEmpty(kotlin.coroutines.Continuation<? super kotlin.Unit> p0) {
        return null;
    }
}