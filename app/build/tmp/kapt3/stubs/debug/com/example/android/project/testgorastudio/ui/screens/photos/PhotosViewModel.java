package com.example.android.project.testgorastudio.ui.screens.photos;

import androidx.lifecycle.ViewModel;
import com.example.android.project.testgorastudio.domen.interactors.GetPhotos;
import com.example.android.project.testgorastudio.domen.usecase.UseCase;
import dagger.hilt.android.lifecycle.HiltViewModel;
import kotlinx.coroutines.Dispatchers;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0002\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fR\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00070\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u0010"}, d2 = {"Lcom/example/android/project/testgorastudio/ui/screens/photos/PhotosViewModel;", "Landroidx/lifecycle/ViewModel;", "getPhotos", "Lcom/example/android/project/testgorastudio/domen/interactors/GetPhotos;", "(Lcom/example/android/project/testgorastudio/domen/interactors/GetPhotos;)V", "_uiState", "Lkotlinx/coroutines/flow/MutableStateFlow;", "Lcom/example/android/project/testgorastudio/ui/screens/photos/State;", "initialState", "uiState", "Lkotlinx/coroutines/flow/StateFlow;", "getUiState", "()Lkotlinx/coroutines/flow/StateFlow;", "", "userId", "", "app_debug"})
public final class PhotosViewModel extends androidx.lifecycle.ViewModel {
    private final com.example.android.project.testgorastudio.domen.interactors.GetPhotos getPhotos = null;
    private final com.example.android.project.testgorastudio.ui.screens.photos.State initialState = null;
    private final kotlinx.coroutines.flow.MutableStateFlow<com.example.android.project.testgorastudio.ui.screens.photos.State> _uiState = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.StateFlow<com.example.android.project.testgorastudio.ui.screens.photos.State> uiState = null;
    
    @javax.inject.Inject()
    public PhotosViewModel(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.domen.interactors.GetPhotos getPhotos) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.StateFlow<com.example.android.project.testgorastudio.ui.screens.photos.State> getUiState() {
        return null;
    }
    
    public final void getPhotos(int userId) {
    }
}