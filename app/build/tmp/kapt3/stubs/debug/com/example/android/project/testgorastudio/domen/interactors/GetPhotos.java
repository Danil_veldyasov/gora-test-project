package com.example.android.project.testgorastudio.domen.interactors;

import android.provider.ContactsContract;
import com.example.android.project.testgorastudio.domen.model.Photo;
import com.example.android.project.testgorastudio.domen.repository.PhotoRepository;
import com.example.android.project.testgorastudio.domen.usecase.UseCase;
import kotlinx.coroutines.flow.Flow;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u001a\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002\u0012\u0004\u0012\u00020\u00050\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ%\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u00022\u0006\u0010\n\u001a\u00020\u0005H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\f"}, d2 = {"Lcom/example/android/project/testgorastudio/domen/interactors/GetPhotos;", "Lcom/example/android/project/testgorastudio/domen/usecase/UseCase;", "Lkotlinx/coroutines/flow/Flow;", "", "Lcom/example/android/project/testgorastudio/domen/model/Photo;", "", "photoRepository", "Lcom/example/android/project/testgorastudio/domen/repository/PhotoRepository;", "(Lcom/example/android/project/testgorastudio/domen/repository/PhotoRepository;)V", "run", "userId", "(ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class GetPhotos extends com.example.android.project.testgorastudio.domen.usecase.UseCase<kotlinx.coroutines.flow.Flow<? extends java.util.List<? extends com.example.android.project.testgorastudio.domen.model.Photo>>, java.lang.Integer> {
    private final com.example.android.project.testgorastudio.domen.repository.PhotoRepository photoRepository = null;
    
    @javax.inject.Inject()
    public GetPhotos(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.domen.repository.PhotoRepository photoRepository) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object run(int userId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlinx.coroutines.flow.Flow<? extends java.util.List<com.example.android.project.testgorastudio.domen.model.Photo>>> p1) {
        return null;
    }
}