package com.example.android.project.testgorastudio.data.repository.mappers;

import com.example.android.project.testgorastudio.data.Mapper;
import com.example.android.project.testgorastudio.data.model.UserDTO;
import com.example.android.project.testgorastudio.domen.model.User;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u001e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003`\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\u0011\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0002H\u0096\u0002\u00a8\u0006\b"}, d2 = {"Lcom/example/android/project/testgorastudio/data/repository/mappers/UserDTOtoUser;", "Lkotlin/Function1;", "Lcom/example/android/project/testgorastudio/data/model/UserDTO;", "Lcom/example/android/project/testgorastudio/domen/model/User;", "Lcom/example/android/project/testgorastudio/data/Mapper;", "()V", "invoke", "userDTO", "app_debug"})
public final class UserDTOtoUser implements kotlin.jvm.functions.Function1<com.example.android.project.testgorastudio.data.model.UserDTO, com.example.android.project.testgorastudio.domen.model.User> {
    
    public UserDTOtoUser() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.android.project.testgorastudio.domen.model.User invoke(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.data.model.UserDTO userDTO) {
        return null;
    }
}