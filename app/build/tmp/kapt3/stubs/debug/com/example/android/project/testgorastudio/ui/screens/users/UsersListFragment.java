package com.example.android.project.testgorastudio.ui.screens.users;

import android.os.Bundle;
import android.util.Log;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.example.android.project.testgorastudio.R;
import com.example.android.project.testgorastudio.databinding.FragmentUsersListBinding;
import com.example.android.project.testgorastudio.ui.screens.photos.PhotosListFragment;
import com.example.android.project.testgorastudio.ui.screens.users.adapter.UserRecyclerAdapter;
import dagger.hilt.android.AndroidEntryPoint;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J&\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\u001a\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u00102\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\u0010\u0010\u001a\u001a\u00020\u00182\u0006\u0010\u001b\u001a\u00020\u001cH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u001d"}, d2 = {"Lcom/example/android/project/testgorastudio/ui/screens/users/UsersListFragment;", "Landroidx/fragment/app/Fragment;", "()V", "TAG", "", "adapter", "Lcom/example/android/project/testgorastudio/ui/screens/users/adapter/UserRecyclerAdapter;", "binding", "Lcom/example/android/project/testgorastudio/databinding/FragmentUsersListBinding;", "usersViewModel", "Lcom/example/android/project/testgorastudio/ui/screens/users/UsersViewModel;", "getUsersViewModel", "()Lcom/example/android/project/testgorastudio/ui/screens/users/UsersViewModel;", "usersViewModel$delegate", "Lkotlin/Lazy;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "", "view", "render", "usersViewState", "Lcom/example/android/project/testgorastudio/ui/screens/users/State;", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class UsersListFragment extends androidx.fragment.app.Fragment {
    private final kotlin.Lazy usersViewModel$delegate = null;
    private com.example.android.project.testgorastudio.databinding.FragmentUsersListBinding binding;
    private com.example.android.project.testgorastudio.ui.screens.users.adapter.UserRecyclerAdapter adapter;
    private final java.lang.String TAG = "UsersListFragment";
    
    public UsersListFragment() {
        super();
    }
    
    private final com.example.android.project.testgorastudio.ui.screens.users.UsersViewModel getUsersViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void render(com.example.android.project.testgorastudio.ui.screens.users.State usersViewState) {
    }
}