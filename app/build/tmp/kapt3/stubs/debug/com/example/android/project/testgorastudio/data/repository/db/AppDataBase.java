package com.example.android.project.testgorastudio.data.repository.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import com.example.android.project.testgorastudio.data.model.PhotoDTO;
import com.example.android.project.testgorastudio.data.model.UserDTO;
import com.example.android.project.testgorastudio.data.repository.photo.PhotoDao;
import com.example.android.project.testgorastudio.data.repository.users.UserDao;

@androidx.room.Database(entities = {com.example.android.project.testgorastudio.data.model.UserDTO.class, com.example.android.project.testgorastudio.data.model.PhotoDTO.class}, version = 1, exportSchema = false)
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\u0007"}, d2 = {"Lcom/example/android/project/testgorastudio/data/repository/db/AppDataBase;", "Landroidx/room/RoomDatabase;", "()V", "getPhotoDao", "Lcom/example/android/project/testgorastudio/data/repository/photo/PhotoDao;", "getUserDao", "Lcom/example/android/project/testgorastudio/data/repository/users/UserDao;", "app_debug"})
public abstract class AppDataBase extends androidx.room.RoomDatabase {
    
    public AppDataBase() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.example.android.project.testgorastudio.data.repository.users.UserDao getUserDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.example.android.project.testgorastudio.data.repository.photo.PhotoDao getPhotoDao();
}