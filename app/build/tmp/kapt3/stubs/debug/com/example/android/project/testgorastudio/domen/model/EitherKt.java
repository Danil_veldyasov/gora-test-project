package com.example.android.project.testgorastudio.domen.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u001aH\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0004\"\u0004\b\u0002\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00040\u00012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00030\u0001\u001aT\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u0002H\b\u0012\u0004\u0012\u0002H\t0\u0007\"\u0004\b\u0000\u0010\t\"\u0004\b\u0001\u0010\b\"\u0004\b\u0002\u0010\n*\u000e\u0012\u0004\u0012\u0002H\b\u0012\u0004\u0012\u0002H\n0\u00072\u001e\u0010\u000b\u001a\u001a\u0012\u0004\u0012\u0002H\n\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\b\u0012\u0004\u0012\u0002H\t0\u00070\u0001\u001a/\u0010\f\u001a\u0002H\n\"\u0004\b\u0000\u0010\b\"\u0004\b\u0001\u0010\n*\u000e\u0012\u0004\u0012\u0002H\b\u0012\u0004\u0012\u0002H\n0\u00072\u0006\u0010\r\u001a\u0002H\n\u00a2\u0006\u0002\u0010\u000e\u001aH\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u0002H\b\u0012\u0004\u0012\u0002H\t0\u0007\"\u0004\b\u0000\u0010\t\"\u0004\b\u0001\u0010\b\"\u0004\b\u0002\u0010\n*\u000e\u0012\u0004\u0012\u0002H\b\u0012\u0004\u0012\u0002H\n0\u00072\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\t0\u0001\u001aQ\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u0002H\b\u0012\u0004\u0012\u0002H\n0\u0007\"\u0004\b\u0000\u0010\b\"\u0004\b\u0001\u0010\n*\u000e\u0012\u0004\u0012\u0002H\b\u0012\u0004\u0012\u0002H\n0\u00072!\u0010\u000b\u001a\u001d\u0012\u0013\u0012\u0011H\b\u00a2\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\u0001\u001aQ\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u0002H\b\u0012\u0004\u0012\u0002H\n0\u0007\"\u0004\b\u0000\u0010\b\"\u0004\b\u0001\u0010\n*\u000e\u0012\u0004\u0012\u0002H\b\u0012\u0004\u0012\u0002H\n0\u00072!\u0010\u000b\u001a\u001d\u0012\u0013\u0012\u0011H\n\u00a2\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u00140\u0001\u00a8\u0006\u0017"}, d2 = {"c", "Lkotlin/Function1;", "A", "C", "B", "f", "flatMap", "Lcom/example/android/project/testgorastudio/domen/model/Either;", "L", "T", "R", "fn", "getOrElse", "value", "(Lcom/example/android/project/testgorastudio/domen/model/Either;Ljava/lang/Object;)Ljava/lang/Object;", "map", "onFailure", "Lkotlin/ParameterName;", "name", "failure", "", "onSuccess", "success", "app_debug"})
public final class EitherKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final <A extends java.lang.Object, B extends java.lang.Object, C extends java.lang.Object>kotlin.jvm.functions.Function1<A, C> c(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super A, ? extends B> $this$c, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super B, ? extends C> f) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final <T extends java.lang.Object, L extends java.lang.Object, R extends java.lang.Object>com.example.android.project.testgorastudio.domen.model.Either<L, T> flatMap(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.domen.model.Either<? extends L, ? extends R> $this$flatMap, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super R, ? extends com.example.android.project.testgorastudio.domen.model.Either<? extends L, ? extends T>> fn) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final <T extends java.lang.Object, L extends java.lang.Object, R extends java.lang.Object>com.example.android.project.testgorastudio.domen.model.Either<L, T> map(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.domen.model.Either<? extends L, ? extends R> $this$map, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super R, ? extends T> fn) {
        return null;
    }
    
    public static final <L extends java.lang.Object, R extends java.lang.Object>R getOrElse(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.domen.model.Either<? extends L, ? extends R> $this$getOrElse, R value) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final <L extends java.lang.Object, R extends java.lang.Object>com.example.android.project.testgorastudio.domen.model.Either<L, R> onFailure(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.domen.model.Either<? extends L, ? extends R> $this$onFailure, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super L, kotlin.Unit> fn) {
        return null;
    }
    
    /**
     * Right-biased onSuccess() FP convention dictates that when this class is Right, it'll perform
     * the onSuccess functionality passed as a parameter, but, overall will still return an either
     * object so you chain calls.
     */
    @org.jetbrains.annotations.NotNull()
    public static final <L extends java.lang.Object, R extends java.lang.Object>com.example.android.project.testgorastudio.domen.model.Either<L, R> onSuccess(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.domen.model.Either<? extends L, ? extends R> $this$onSuccess, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super R, kotlin.Unit> fn) {
        return null;
    }
}