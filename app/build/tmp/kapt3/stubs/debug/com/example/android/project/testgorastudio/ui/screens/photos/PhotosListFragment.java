package com.example.android.project.testgorastudio.ui.screens.photos;

import android.os.Bundle;
import android.util.Log;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.example.android.project.testgorastudio.R;
import com.example.android.project.testgorastudio.databinding.FragmentPhotosListBinding;
import com.example.android.project.testgorastudio.databinding.FragmentUsersListBinding;
import com.example.android.project.testgorastudio.ui.screens.photos.adapter.PhotoRecyclerAdapter;
import dagger.hilt.android.AndroidEntryPoint;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u0000 !2\u00020\u0001:\u0001!B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J&\u0010\u0016\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\u001a\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u00172\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\u0010\u0010\u001e\u001a\u00020\u00132\u0006\u0010\u001f\u001a\u00020 H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\fR\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0011\u00a8\u0006\""}, d2 = {"Lcom/example/android/project/testgorastudio/ui/screens/photos/PhotosListFragment;", "Landroidx/fragment/app/Fragment;", "()V", "TAG", "", "adapter", "Lcom/example/android/project/testgorastudio/ui/screens/photos/adapter/PhotoRecyclerAdapter;", "binding", "Lcom/example/android/project/testgorastudio/databinding/FragmentPhotosListBinding;", "photosViewModel", "Lcom/example/android/project/testgorastudio/ui/screens/photos/PhotosViewModel;", "getPhotosViewModel", "()Lcom/example/android/project/testgorastudio/ui/screens/photos/PhotosViewModel;", "photosViewModel$delegate", "Lkotlin/Lazy;", "userId", "", "Ljava/lang/Integer;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onViewCreated", "view", "render", "photosViewState", "Lcom/example/android/project/testgorastudio/ui/screens/photos/State;", "Companion", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class PhotosListFragment extends androidx.fragment.app.Fragment {
    private final kotlin.Lazy photosViewModel$delegate = null;
    private com.example.android.project.testgorastudio.databinding.FragmentPhotosListBinding binding;
    private com.example.android.project.testgorastudio.ui.screens.photos.adapter.PhotoRecyclerAdapter adapter;
    private java.lang.Integer userId;
    private final java.lang.String TAG = "PhotoListFragment";
    @org.jetbrains.annotations.NotNull()
    public static final com.example.android.project.testgorastudio.ui.screens.photos.PhotosListFragment.Companion Companion = null;
    
    public PhotosListFragment() {
        super();
    }
    
    private final com.example.android.project.testgorastudio.ui.screens.photos.PhotosViewModel getPhotosViewModel() {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void render(com.example.android.project.testgorastudio.ui.screens.photos.State photosViewState) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.example.android.project.testgorastudio.ui.screens.photos.PhotosListFragment newInstance(int userId) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"}, d2 = {"Lcom/example/android/project/testgorastudio/ui/screens/photos/PhotosListFragment$Companion;", "", "()V", "newInstance", "Lcom/example/android/project/testgorastudio/ui/screens/photos/PhotosListFragment;", "userId", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.example.android.project.testgorastudio.ui.screens.photos.PhotosListFragment newInstance(int userId) {
            return null;
        }
    }
}