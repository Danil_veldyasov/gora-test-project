package com.example.android.project.testgorastudio.data.repository.photo.datasource;

import com.example.android.project.testgorastudio.data.model.PhotoDTO;
import com.example.android.project.testgorastudio.data.repository.photo.PhotoApi;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\b\u001a\u00020\tH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u000b"}, d2 = {"Lcom/example/android/project/testgorastudio/data/repository/photo/datasource/PhotoNetworkDataSourceImpl;", "Lcom/example/android/project/testgorastudio/data/repository/photo/datasource/PhotoNetworkDataSource;", "photoApi", "Lcom/example/android/project/testgorastudio/data/repository/photo/PhotoApi;", "(Lcom/example/android/project/testgorastudio/data/repository/photo/PhotoApi;)V", "getPhotos", "", "Lcom/example/android/project/testgorastudio/data/model/PhotoDTO;", "userId", "", "(ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class PhotoNetworkDataSourceImpl implements com.example.android.project.testgorastudio.data.repository.photo.datasource.PhotoNetworkDataSource {
    private final com.example.android.project.testgorastudio.data.repository.photo.PhotoApi photoApi = null;
    
    @javax.inject.Inject()
    public PhotoNetworkDataSourceImpl(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.data.repository.photo.PhotoApi photoApi) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getPhotos(int userId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<com.example.android.project.testgorastudio.data.model.PhotoDTO>> p1) {
        return null;
    }
}