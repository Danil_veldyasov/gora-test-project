package com.example.android.project.testgorastudio.data.repository.users.datasource;

import com.example.android.project.testgorastudio.data.model.UserDTO;
import com.example.android.project.testgorastudio.data.repository.users.UserApi;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\t"}, d2 = {"Lcom/example/android/project/testgorastudio/data/repository/users/datasource/UserNetworkDataSourceImpl;", "Lcom/example/android/project/testgorastudio/data/repository/users/datasource/UserNetworkDataSource;", "userApi", "Lcom/example/android/project/testgorastudio/data/repository/users/UserApi;", "(Lcom/example/android/project/testgorastudio/data/repository/users/UserApi;)V", "getUsers", "", "Lcom/example/android/project/testgorastudio/data/model/UserDTO;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class UserNetworkDataSourceImpl implements com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSource {
    private final com.example.android.project.testgorastudio.data.repository.users.UserApi userApi = null;
    
    @javax.inject.Inject()
    public UserNetworkDataSourceImpl(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.data.repository.users.UserApi userApi) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getUsers(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<com.example.android.project.testgorastudio.data.model.UserDTO>> p0) {
        return null;
    }
}