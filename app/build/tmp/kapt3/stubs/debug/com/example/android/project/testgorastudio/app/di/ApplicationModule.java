package com.example.android.project.testgorastudio.app.di;

import android.content.Context;
import androidx.room.Room;
import com.example.android.project.testgorastudio.data.repository.users.UserDao;
import com.example.android.project.testgorastudio.data.repository.db.AppDataBase;
import com.example.android.project.testgorastudio.data.repository.photo.PhotoApi;
import com.example.android.project.testgorastudio.data.repository.photo.PhotoDao;
import com.example.android.project.testgorastudio.data.repository.photo.PhotoRepositoryImpl;
import com.example.android.project.testgorastudio.data.repository.photo.datasource.PhotoNetworkDataSource;
import com.example.android.project.testgorastudio.data.repository.photo.datasource.PhotoNetworkDataSourceImpl;
import com.example.android.project.testgorastudio.data.repository.users.UserApi;
import com.example.android.project.testgorastudio.data.repository.users.UserRepositoryImpl;
import com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSource;
import com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSourceImpl;
import com.example.android.project.testgorastudio.domen.interactors.GetPhotos;
import com.example.android.project.testgorastudio.domen.interactors.GetUsers;
import com.example.android.project.testgorastudio.domen.repository.PhotoRepository;
import com.example.android.project.testgorastudio.domen.repository.UserRepository;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import java.util.concurrent.TimeUnit;

@dagger.hilt.InstallIn(value = {dagger.hilt.components.SingletonComponent.class})
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007J\u0012\u0010\r\u001a\u00020\u000e2\b\b\u0001\u0010\u000f\u001a\u00020\u0010H\u0007J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\nH\u0007J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0007J\u0018\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u0012H\u0007J\b\u0010\u001b\u001a\u00020\fH\u0007J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u000b\u001a\u00020\fH\u0007J\u0012\u0010\u001e\u001a\u00020\u001f2\b\b\u0001\u0010\u000f\u001a\u00020\u0010H\u0007J\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u001dH\u0007J\u0018\u0010#\u001a\u00020\b2\u0006\u0010$\u001a\u00020\u001f2\u0006\u0010\u001a\u001a\u00020!H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"}, d2 = {"Lcom/example/android/project/testgorastudio/app/di/ApplicationModule;", "", "()V", "BASE_URL", "", "provideGetUserUseCase", "Lcom/example/android/project/testgorastudio/domen/interactors/GetUsers;", "userRepository", "Lcom/example/android/project/testgorastudio/domen/repository/UserRepository;", "providePhotoApi", "Lcom/example/android/project/testgorastudio/data/repository/photo/PhotoApi;", "retrofit", "Lretrofit2/Retrofit;", "providePhotoDao", "Lcom/example/android/project/testgorastudio/data/repository/photo/PhotoDao;", "context", "Landroid/content/Context;", "providePhotoNetworkDataSource", "Lcom/example/android/project/testgorastudio/data/repository/photo/datasource/PhotoNetworkDataSource;", "photoApi", "providePhotoPhotoUseCase", "Lcom/example/android/project/testgorastudio/domen/interactors/GetPhotos;", "photoRepository", "Lcom/example/android/project/testgorastudio/domen/repository/PhotoRepository;", "providePhotoRepository", "photoDao", "source", "provideRetrofit", "provideUserApi", "Lcom/example/android/project/testgorastudio/data/repository/users/UserApi;", "provideUserDao", "Lcom/example/android/project/testgorastudio/data/repository/users/UserDao;", "provideUserNetworkDataSource", "Lcom/example/android/project/testgorastudio/data/repository/users/datasource/UserNetworkDataSource;", "userApi", "provideUserRepository", "userDao", "app_debug"})
@dagger.Module()
public final class ApplicationModule {
    @org.jetbrains.annotations.NotNull()
    public static final com.example.android.project.testgorastudio.app.di.ApplicationModule INSTANCE = null;
    private static final java.lang.String BASE_URL = "https://jsonplaceholder.typicode.com/";
    
    private ApplicationModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final retrofit2.Retrofit provideRetrofit() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.example.android.project.testgorastudio.domen.repository.PhotoRepository providePhotoRepository(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.data.repository.photo.PhotoDao photoDao, @org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.data.repository.photo.datasource.PhotoNetworkDataSource source) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.example.android.project.testgorastudio.domen.repository.UserRepository provideUserRepository(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.data.repository.users.UserDao userDao, @org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSource source) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.example.android.project.testgorastudio.data.repository.users.UserDao provideUserDao(@org.jetbrains.annotations.NotNull()
    @dagger.hilt.android.qualifiers.ApplicationContext()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.example.android.project.testgorastudio.data.repository.photo.PhotoDao providePhotoDao(@org.jetbrains.annotations.NotNull()
    @dagger.hilt.android.qualifiers.ApplicationContext()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSource provideUserNetworkDataSource(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.data.repository.users.UserApi userApi) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.example.android.project.testgorastudio.data.repository.photo.datasource.PhotoNetworkDataSource providePhotoNetworkDataSource(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.data.repository.photo.PhotoApi photoApi) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.example.android.project.testgorastudio.data.repository.users.UserApi provideUserApi(@org.jetbrains.annotations.NotNull()
    retrofit2.Retrofit retrofit) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.example.android.project.testgorastudio.data.repository.photo.PhotoApi providePhotoApi(@org.jetbrains.annotations.NotNull()
    retrofit2.Retrofit retrofit) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.example.android.project.testgorastudio.domen.interactors.GetUsers provideGetUserUseCase(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.domen.repository.UserRepository userRepository) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.example.android.project.testgorastudio.domen.interactors.GetPhotos providePhotoPhotoUseCase(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.domen.repository.PhotoRepository photoRepository) {
        return null;
    }
}