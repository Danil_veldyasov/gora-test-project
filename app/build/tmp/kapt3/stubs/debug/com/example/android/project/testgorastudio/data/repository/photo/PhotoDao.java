package com.example.android.project.testgorastudio.data.repository.photo;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import com.example.android.project.testgorastudio.data.model.PhotoDTO;
import com.example.android.project.testgorastudio.data.model.UserDTO;
import kotlinx.coroutines.flow.Flow;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\bg\u0018\u00002\u00020\u0001J\u001c\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u0007H\'J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\u0007H\'J\u0016\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\'\u00a8\u0006\r"}, d2 = {"Lcom/example/android/project/testgorastudio/data/repository/photo/PhotoDao;", "", "getPhotos", "Lkotlinx/coroutines/flow/Flow;", "", "Lcom/example/android/project/testgorastudio/data/model/PhotoDTO;", "userId", "", "hasPhotos", "", "save", "", "photoDTO", "app_debug"})
public abstract interface PhotoDao {
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract void save(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.android.project.testgorastudio.data.model.PhotoDTO> photoDTO);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM photos WHERE albumId = :userId")
    public abstract kotlinx.coroutines.flow.Flow<java.util.List<com.example.android.project.testgorastudio.data.model.PhotoDTO>> getPhotos(int userId);
    
    @androidx.room.Query(value = "SELECT count(*)>0  FROM photos WHERE albumId = :userId")
    public abstract boolean hasPhotos(int userId);
}