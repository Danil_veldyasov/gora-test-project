package com.example.android.project.testgorastudio.domen.exception;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0004\u0007\b\t\n\u00a8\u0006\u000b"}, d2 = {"Lcom/example/android/project/testgorastudio/domen/exception/FailureOrLoading;", "", "()V", "FeatureFailureOrLoading", "Loading", "NetworkConnection", "ServerError", "Lcom/example/android/project/testgorastudio/domen/exception/FailureOrLoading$NetworkConnection;", "Lcom/example/android/project/testgorastudio/domen/exception/FailureOrLoading$ServerError;", "Lcom/example/android/project/testgorastudio/domen/exception/FailureOrLoading$FeatureFailureOrLoading;", "Lcom/example/android/project/testgorastudio/domen/exception/FailureOrLoading$Loading;", "app_debug"})
public abstract class FailureOrLoading {
    
    private FailureOrLoading() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/example/android/project/testgorastudio/domen/exception/FailureOrLoading$NetworkConnection;", "Lcom/example/android/project/testgorastudio/domen/exception/FailureOrLoading;", "()V", "app_debug"})
    public static final class NetworkConnection extends com.example.android.project.testgorastudio.domen.exception.FailureOrLoading {
        @org.jetbrains.annotations.NotNull()
        public static final com.example.android.project.testgorastudio.domen.exception.FailureOrLoading.NetworkConnection INSTANCE = null;
        
        private NetworkConnection() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/example/android/project/testgorastudio/domen/exception/FailureOrLoading$ServerError;", "Lcom/example/android/project/testgorastudio/domen/exception/FailureOrLoading;", "()V", "app_debug"})
    public static final class ServerError extends com.example.android.project.testgorastudio.domen.exception.FailureOrLoading {
        @org.jetbrains.annotations.NotNull()
        public static final com.example.android.project.testgorastudio.domen.exception.FailureOrLoading.ServerError INSTANCE = null;
        
        private ServerError() {
            super();
        }
    }
    
    /**
     * * Extend this class for feature specific failures.
     */
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b&\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/example/android/project/testgorastudio/domen/exception/FailureOrLoading$FeatureFailureOrLoading;", "Lcom/example/android/project/testgorastudio/domen/exception/FailureOrLoading;", "()V", "app_debug"})
    public static abstract class FeatureFailureOrLoading extends com.example.android.project.testgorastudio.domen.exception.FailureOrLoading {
        
        public FeatureFailureOrLoading() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b&\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/example/android/project/testgorastudio/domen/exception/FailureOrLoading$Loading;", "Lcom/example/android/project/testgorastudio/domen/exception/FailureOrLoading;", "()V", "app_debug"})
    public static abstract class Loading extends com.example.android.project.testgorastudio.domen.exception.FailureOrLoading {
        
        public Loading() {
            super();
        }
    }
}