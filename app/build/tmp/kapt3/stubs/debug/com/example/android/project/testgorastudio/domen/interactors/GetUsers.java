package com.example.android.project.testgorastudio.domen.interactors;

import com.example.android.project.testgorastudio.domen.exception.FailureOrLoading;
import com.example.android.project.testgorastudio.domen.model.Either;
import com.example.android.project.testgorastudio.domen.model.User;
import com.example.android.project.testgorastudio.domen.repository.UserRepository;
import com.example.android.project.testgorastudio.domen.usecase.UseCase;
import kotlinx.coroutines.flow.Flow;
import java.io.IOException;
import java.lang.Error;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u001a\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002\u0012\u0004\u0012\u00020\u00050\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ%\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u00022\u0006\u0010\n\u001a\u00020\u0005H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\f"}, d2 = {"Lcom/example/android/project/testgorastudio/domen/interactors/GetUsers;", "Lcom/example/android/project/testgorastudio/domen/usecase/UseCase;", "Lkotlinx/coroutines/flow/Flow;", "", "Lcom/example/android/project/testgorastudio/domen/model/User;", "Lcom/example/android/project/testgorastudio/domen/usecase/UseCase$None;", "userRepository", "Lcom/example/android/project/testgorastudio/domen/repository/UserRepository;", "(Lcom/example/android/project/testgorastudio/domen/repository/UserRepository;)V", "run", "params", "(Lcom/example/android/project/testgorastudio/domen/usecase/UseCase$None;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class GetUsers extends com.example.android.project.testgorastudio.domen.usecase.UseCase<kotlinx.coroutines.flow.Flow<? extends java.util.List<? extends com.example.android.project.testgorastudio.domen.model.User>>, com.example.android.project.testgorastudio.domen.usecase.UseCase.None> {
    private final com.example.android.project.testgorastudio.domen.repository.UserRepository userRepository = null;
    
    @javax.inject.Inject()
    public GetUsers(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.domen.repository.UserRepository userRepository) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object run(@org.jetbrains.annotations.NotNull()
    com.example.android.project.testgorastudio.domen.usecase.UseCase.None params, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlinx.coroutines.flow.Flow<? extends java.util.List<com.example.android.project.testgorastudio.domen.model.User>>> p1) {
        return null;
    }
}