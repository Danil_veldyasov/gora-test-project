// Generated by Dagger (https://dagger.dev).
package com.example.android.project.testgorastudio.data.repository.users.datasource;

import com.example.android.project.testgorastudio.data.repository.users.UserApi;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class UserNetworkDataSourceImpl_Factory implements Factory<UserNetworkDataSourceImpl> {
  private final Provider<UserApi> userApiProvider;

  public UserNetworkDataSourceImpl_Factory(Provider<UserApi> userApiProvider) {
    this.userApiProvider = userApiProvider;
  }

  @Override
  public UserNetworkDataSourceImpl get() {
    return newInstance(userApiProvider.get());
  }

  public static UserNetworkDataSourceImpl_Factory create(Provider<UserApi> userApiProvider) {
    return new UserNetworkDataSourceImpl_Factory(userApiProvider);
  }

  public static UserNetworkDataSourceImpl newInstance(UserApi userApi) {
    return new UserNetworkDataSourceImpl(userApi);
  }
}
