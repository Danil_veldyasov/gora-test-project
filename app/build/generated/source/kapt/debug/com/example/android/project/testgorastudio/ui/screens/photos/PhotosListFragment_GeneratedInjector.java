package com.example.android.project.testgorastudio.ui.screens.photos;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = PhotosListFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface PhotosListFragment_GeneratedInjector {
  void injectPhotosListFragment(PhotosListFragment photosListFragment);
}
