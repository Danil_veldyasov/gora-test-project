// Generated by Dagger (https://dagger.dev).
package com.example.android.project.testgorastudio.data.repository.users;

import com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSource;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class UserRepositoryImpl_Factory implements Factory<UserRepositoryImpl> {
  private final Provider<UserDao> userDaoProvider;

  private final Provider<UserNetworkDataSource> dataSourceProvider;

  public UserRepositoryImpl_Factory(Provider<UserDao> userDaoProvider,
      Provider<UserNetworkDataSource> dataSourceProvider) {
    this.userDaoProvider = userDaoProvider;
    this.dataSourceProvider = dataSourceProvider;
  }

  @Override
  public UserRepositoryImpl get() {
    return newInstance(userDaoProvider.get(), dataSourceProvider.get());
  }

  public static UserRepositoryImpl_Factory create(Provider<UserDao> userDaoProvider,
      Provider<UserNetworkDataSource> dataSourceProvider) {
    return new UserRepositoryImpl_Factory(userDaoProvider, dataSourceProvider);
  }

  public static UserRepositoryImpl newInstance(UserDao userDao, UserNetworkDataSource dataSource) {
    return new UserRepositoryImpl(userDao, dataSource);
  }
}
