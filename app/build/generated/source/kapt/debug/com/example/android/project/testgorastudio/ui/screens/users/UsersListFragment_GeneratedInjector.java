package com.example.android.project.testgorastudio.ui.screens.users;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = UsersListFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface UsersListFragment_GeneratedInjector {
  void injectUsersListFragment(UsersListFragment usersListFragment);
}
