// Generated by Dagger (https://dagger.dev).
package com.example.android.project.testgorastudio.app.di;

import com.example.android.project.testgorastudio.data.repository.users.UserDao;
import com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSource;
import com.example.android.project.testgorastudio.domen.repository.UserRepository;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ApplicationModule_ProvideUserRepositoryFactory implements Factory<UserRepository> {
  private final Provider<UserDao> userDaoProvider;

  private final Provider<UserNetworkDataSource> sourceProvider;

  public ApplicationModule_ProvideUserRepositoryFactory(Provider<UserDao> userDaoProvider,
      Provider<UserNetworkDataSource> sourceProvider) {
    this.userDaoProvider = userDaoProvider;
    this.sourceProvider = sourceProvider;
  }

  @Override
  public UserRepository get() {
    return provideUserRepository(userDaoProvider.get(), sourceProvider.get());
  }

  public static ApplicationModule_ProvideUserRepositoryFactory create(
      Provider<UserDao> userDaoProvider, Provider<UserNetworkDataSource> sourceProvider) {
    return new ApplicationModule_ProvideUserRepositoryFactory(userDaoProvider, sourceProvider);
  }

  public static UserRepository provideUserRepository(UserDao userDao,
      UserNetworkDataSource source) {
    return Preconditions.checkNotNullFromProvides(ApplicationModule.INSTANCE.provideUserRepository(userDao, source));
  }
}
