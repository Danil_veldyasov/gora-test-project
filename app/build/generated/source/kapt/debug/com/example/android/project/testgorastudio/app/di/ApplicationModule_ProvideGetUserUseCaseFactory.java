// Generated by Dagger (https://dagger.dev).
package com.example.android.project.testgorastudio.app.di;

import com.example.android.project.testgorastudio.domen.interactors.GetUsers;
import com.example.android.project.testgorastudio.domen.repository.UserRepository;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ApplicationModule_ProvideGetUserUseCaseFactory implements Factory<GetUsers> {
  private final Provider<UserRepository> userRepositoryProvider;

  public ApplicationModule_ProvideGetUserUseCaseFactory(
      Provider<UserRepository> userRepositoryProvider) {
    this.userRepositoryProvider = userRepositoryProvider;
  }

  @Override
  public GetUsers get() {
    return provideGetUserUseCase(userRepositoryProvider.get());
  }

  public static ApplicationModule_ProvideGetUserUseCaseFactory create(
      Provider<UserRepository> userRepositoryProvider) {
    return new ApplicationModule_ProvideGetUserUseCaseFactory(userRepositoryProvider);
  }

  public static GetUsers provideGetUserUseCase(UserRepository userRepository) {
    return Preconditions.checkNotNullFromProvides(ApplicationModule.INSTANCE.provideGetUserUseCase(userRepository));
  }
}
