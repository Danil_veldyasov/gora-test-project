// Generated by Dagger (https://dagger.dev).
package com.example.android.project.testgorastudio.app.di;

import com.example.android.project.testgorastudio.data.repository.users.UserApi;
import com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSource;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ApplicationModule_ProvideUserNetworkDataSourceFactory implements Factory<UserNetworkDataSource> {
  private final Provider<UserApi> userApiProvider;

  public ApplicationModule_ProvideUserNetworkDataSourceFactory(Provider<UserApi> userApiProvider) {
    this.userApiProvider = userApiProvider;
  }

  @Override
  public UserNetworkDataSource get() {
    return provideUserNetworkDataSource(userApiProvider.get());
  }

  public static ApplicationModule_ProvideUserNetworkDataSourceFactory create(
      Provider<UserApi> userApiProvider) {
    return new ApplicationModule_ProvideUserNetworkDataSourceFactory(userApiProvider);
  }

  public static UserNetworkDataSource provideUserNetworkDataSource(UserApi userApi) {
    return Preconditions.checkNotNullFromProvides(ApplicationModule.INSTANCE.provideUserNetworkDataSource(userApi));
  }
}
