package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.components.SingletonComponent",
    modules = "com.example.android.project.testgorastudio.app.di.ApplicationModule"
)
public class _com_example_android_project_testgorastudio_app_di_ApplicationModule {
}
