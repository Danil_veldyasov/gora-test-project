package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ViewModelComponent",
    modules = "com.example.android.project.testgorastudio.ui.screens.photos.PhotosViewModel_HiltModules.BindsModule"
)
public class _com_example_android_project_testgorastudio_ui_screens_photos_PhotosViewModel_HiltModules_BindsModule {
}
