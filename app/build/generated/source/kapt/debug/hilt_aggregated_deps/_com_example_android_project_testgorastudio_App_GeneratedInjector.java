package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.components.SingletonComponent",
    entryPoints = "com.example.android.project.testgorastudio.App_GeneratedInjector"
)
public class _com_example_android_project_testgorastudio_App_GeneratedInjector {
}
