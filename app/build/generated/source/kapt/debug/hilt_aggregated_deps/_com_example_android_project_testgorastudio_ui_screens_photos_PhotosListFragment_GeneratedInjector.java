package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.FragmentComponent",
    entryPoints = "com.example.android.project.testgorastudio.ui.screens.photos.PhotosListFragment_GeneratedInjector"
)
public class _com_example_android_project_testgorastudio_ui_screens_photos_PhotosListFragment_GeneratedInjector {
}
