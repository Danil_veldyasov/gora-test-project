package com.example.android.project.testgorastudio.data.repository.photo.datasource

import com.example.android.project.testgorastudio.data.model.PhotoDTO

interface PhotoNetworkDataSource {
    suspend fun getPhotos(userId: Int): List<PhotoDTO>
}