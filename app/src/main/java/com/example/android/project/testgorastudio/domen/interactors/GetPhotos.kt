package com.example.android.project.testgorastudio.domen.interactors

import android.provider.ContactsContract
import com.example.android.project.testgorastudio.domen.model.Photo
import com.example.android.project.testgorastudio.domen.repository.PhotoRepository
import com.example.android.project.testgorastudio.domen.usecase.UseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPhotos @Inject constructor(
    private val photoRepository: PhotoRepository
): UseCase<Flow<List<Photo>>, Int>() {

    override suspend fun run(userId: Int): Flow<List<Photo>> {
        return photoRepository.getPhotos(userId)
    }
}