package com.example.android.project.testgorastudio.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "photos")
data class PhotoDTO (
    @PrimaryKey
    val id: Int,
    val albumId: Int,
    val title: String,
    val url: String
    )