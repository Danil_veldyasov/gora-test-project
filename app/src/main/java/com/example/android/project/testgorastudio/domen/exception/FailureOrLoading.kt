package com.example.android.project.testgorastudio.domen.exception

sealed class FailureOrLoading {
    object NetworkConnection : FailureOrLoading()
    object ServerError : FailureOrLoading()

    /** * Extend this class for feature specific failures.*/
    abstract class FeatureFailureOrLoading: FailureOrLoading()
    abstract class Loading: FailureOrLoading()
}
