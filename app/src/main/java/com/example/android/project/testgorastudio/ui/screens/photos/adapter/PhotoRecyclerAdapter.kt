package com.example.android.project.testgorastudio.ui.screens.photos.adapter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.android.project.testgorastudio.R
import com.example.android.project.testgorastudio.domen.model.Photo
import com.example.android.project.testgorastudio.ui.screens.photos.holder.PhotoItemHolder


class PhotoRecyclerAdapter(val context: Context): RecyclerView.Adapter<PhotoItemHolder>() {
    private var listPhoto: List<Photo> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoItemHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.photo_item, parent, false)
        return PhotoItemHolder(itemView)
    }

    override fun onBindViewHolder(holder: PhotoItemHolder, position: Int) {
        val url = GlideUrl(
            listPhoto[position].url, LazyHeaders.Builder()
                .addHeader("User-Agent", "your-user-agent")
                .build()
        )
        Glide.with(context)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.drawable.ic_sand_clock)
            .error(R.drawable.ic_browser)
            .into(holder.image)
        holder.title.text = listPhoto[position].title

    }

    override fun getItemCount() = listPhoto.size

    fun setPhotos(photos: List<Photo>){
        listPhoto = photos
    }
}