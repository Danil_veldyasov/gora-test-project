package com.example.android.project.testgorastudio.ui.screens.photos

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.android.project.testgorastudio.domen.interactors.GetPhotos
import com.example.android.project.testgorastudio.domen.usecase.UseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PhotosViewModel @Inject constructor(
    private val getPhotos: GetPhotos
): ViewModel() {

    private val initialState: State = State.Loading
    private val _uiState : MutableStateFlow<State> = MutableStateFlow(initialState)
    val uiState = _uiState.asStateFlow()

    fun getPhotos(userId: Int){
        viewModelScope.launch(Dispatchers.IO) {

            _uiState.emit(State.Loading)
            try {
                getPhotos.run(userId).collect {
                    _uiState.emit(State.PhotosLoaded(it))
                }
            }
            catch (e: java.lang.Exception) {
                _uiState.emit(State.Error(e.message?: "Unknown error"))
            }

        }
    }
}