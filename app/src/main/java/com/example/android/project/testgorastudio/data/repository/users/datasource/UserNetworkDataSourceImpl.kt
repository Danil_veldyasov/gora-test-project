package com.example.android.project.testgorastudio.data.repository.users.datasource

import com.example.android.project.testgorastudio.data.model.UserDTO
import com.example.android.project.testgorastudio.data.repository.users.UserApi
import javax.inject.Inject

class UserNetworkDataSourceImpl @Inject constructor(
    private val userApi: UserApi
): UserNetworkDataSource {
    override suspend fun getUsers(): List<UserDTO> {
        return userApi.loadUserDTO()
    }
}