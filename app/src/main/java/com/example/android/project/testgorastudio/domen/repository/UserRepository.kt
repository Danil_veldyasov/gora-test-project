package com.example.android.project.testgorastudio.domen.repository

import com.example.android.project.testgorastudio.domen.model.User
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow


interface UserRepository {
    suspend fun getUsers(): Flow<List<User>>
}