package com.example.android.project.testgorastudio.data.repository.photo

import com.example.android.project.testgorastudio.data.model.PhotoDTO
import retrofit2.http.GET
import retrofit2.http.Path

interface PhotoApi {

    @GET("albums/{userId}/photos")
    suspend fun loadPhotosDTO(@Path("userId") userId: Int): List<PhotoDTO>
}