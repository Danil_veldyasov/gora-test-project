package com.example.android.project.testgorastudio.domen.interactors

import com.example.android.project.testgorastudio.domen.exception.FailureOrLoading
import com.example.android.project.testgorastudio.domen.model.Either
import com.example.android.project.testgorastudio.domen.model.User
import com.example.android.project.testgorastudio.domen.repository.UserRepository
import com.example.android.project.testgorastudio.domen.usecase.UseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import java.io.IOException
import java.lang.Error
import javax.inject.Inject

class GetUsers @Inject constructor
    (private val userRepository: UserRepository) : UseCase<Flow<List<User>>, UseCase.None>() {

    override suspend fun run(params: None): Flow<List<User>> {
        return userRepository.getUsers()
    }
}