package com.example.android.project.testgorastudio.app.di

import android.content.Context
import androidx.room.Room
import com.example.android.project.testgorastudio.data.repository.users.UserDao
import com.example.android.project.testgorastudio.data.repository.db.AppDataBase
import com.example.android.project.testgorastudio.data.repository.photo.PhotoApi
import com.example.android.project.testgorastudio.data.repository.photo.PhotoDao
import com.example.android.project.testgorastudio.data.repository.photo.PhotoRepositoryImpl
import com.example.android.project.testgorastudio.data.repository.photo.datasource.PhotoNetworkDataSource
import com.example.android.project.testgorastudio.data.repository.photo.datasource.PhotoNetworkDataSourceImpl
import com.example.android.project.testgorastudio.data.repository.users.UserApi
import com.example.android.project.testgorastudio.data.repository.users.UserRepositoryImpl
import com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSource
import com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSourceImpl
import com.example.android.project.testgorastudio.domen.interactors.GetPhotos
import com.example.android.project.testgorastudio.domen.interactors.GetUsers
import com.example.android.project.testgorastudio.domen.repository.PhotoRepository
import com.example.android.project.testgorastudio.domen.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@InstallIn(SingletonComponent::class)
@Module
object ApplicationModule {
    private const val BASE_URL ="https://jsonplaceholder.typicode.com/"

    @Provides
    fun provideRetrofit(): Retrofit{
        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient()
            .newBuilder()
            .addInterceptor(logInterceptor)
            .callTimeout(5, TimeUnit.SECONDS)
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        return Retrofit.Builder()
            .client(client)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun providePhotoRepository(photoDao: PhotoDao, source: PhotoNetworkDataSource): PhotoRepository{
        return PhotoRepositoryImpl(photoDao, source) }

    @Provides
    fun provideUserRepository(userDao: UserDao, source: UserNetworkDataSource):UserRepository {
        return UserRepositoryImpl(userDao, source) }

    @Provides
    fun provideUserDao(@ApplicationContext context: Context): UserDao {
        return Room.databaseBuilder(context, AppDataBase::class.java, "database")
            .build()
            .getUserDao()
    }

    @Provides
    fun providePhotoDao(@ApplicationContext context: Context): PhotoDao {
        return Room.databaseBuilder(context, AppDataBase::class.java, "database")
            .build()
            .getPhotoDao()
    }

    @Provides
    fun provideUserNetworkDataSource(userApi: UserApi): UserNetworkDataSource = UserNetworkDataSourceImpl(userApi)

    @Provides
    fun providePhotoNetworkDataSource(photoApi: PhotoApi): PhotoNetworkDataSource = PhotoNetworkDataSourceImpl(photoApi)

    @Provides
    fun provideUserApi(retrofit: Retrofit): UserApi = retrofit.create(UserApi::class.java)

    @Provides
    fun providePhotoApi(retrofit: Retrofit): PhotoApi = retrofit.create(PhotoApi::class.java)

    @Provides
    fun provideGetUserUseCase(userRepository: UserRepository) = GetUsers(userRepository)

    @Provides
    fun providePhotoPhotoUseCase(photoRepository: PhotoRepository) = GetPhotos(photoRepository)

}

