package com.example.android.project.testgorastudio.data.repository.users

import com.example.android.project.testgorastudio.data.model.UserDTO
import retrofit2.http.GET

interface UserApi {

    @GET("users")
    suspend fun loadUserDTO(): List<UserDTO>
}