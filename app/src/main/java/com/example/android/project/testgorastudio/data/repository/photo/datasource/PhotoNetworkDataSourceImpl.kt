package com.example.android.project.testgorastudio.data.repository.photo.datasource

import com.example.android.project.testgorastudio.data.model.PhotoDTO
import com.example.android.project.testgorastudio.data.repository.photo.PhotoApi
import javax.inject.Inject

class PhotoNetworkDataSourceImpl @Inject constructor(
    private val photoApi: PhotoApi
): PhotoNetworkDataSource {
    override suspend fun getPhotos(userId: Int): List<PhotoDTO> {
        return photoApi.loadPhotosDTO(userId)
    }
}