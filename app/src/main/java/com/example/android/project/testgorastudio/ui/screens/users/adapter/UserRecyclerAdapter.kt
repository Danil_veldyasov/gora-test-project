package com.example.android.project.testgorastudio.ui.screens.users.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.android.project.testgorastudio.R
import com.example.android.project.testgorastudio.domen.model.User
import com.example.android.project.testgorastudio.ui.screens.users.holder.UserItemHolder

class UserRecyclerAdapter(private val onUserClickListener: OnUserClickListener):
 RecyclerView.Adapter<UserItemHolder>(){
    private var listUsers: List<User> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserItemHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.user_item, parent, false)
        return UserItemHolder(itemView)
    }

    override fun onBindViewHolder(holder: UserItemHolder, position: Int) {
        holder.userName.text = listUsers[position].name
        holder.itemView.setOnClickListener {
            onUserClickListener.onUserClick(listUsers[position].id)
        }
    }

    override fun getItemCount() = listUsers.size

    interface OnUserClickListener{
        fun onUserClick(userId: Int)
    }

    fun setUsers(users: List<User>){
        listUsers = users
    }
}