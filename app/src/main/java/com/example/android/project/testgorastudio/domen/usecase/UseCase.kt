package com.example.android.project.testgorastudio.domen.usecase

import com.example.android.project.testgorastudio.domen.exception.FailureOrLoading
import com.example.android.project.testgorastudio.domen.model.Either

abstract class UseCase<out Type, in Params> where Type : Any {

    abstract suspend fun run(params: Params): Type

    class None
}