package com.example.android.project.testgorastudio.domen.model

data class Photo(
    val albumId: Int,
    val id: Int,
    val title: String,
    val url: String
)
