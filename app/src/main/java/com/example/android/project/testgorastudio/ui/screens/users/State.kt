package com.example.android.project.testgorastudio.ui.screens.users

import com.example.android.project.testgorastudio.domen.model.User

sealed class State(){
    object Loading: State()
    class Error(val message: String): State()
    class UsersLoaded(val listUsers: List<User>): State()
}
