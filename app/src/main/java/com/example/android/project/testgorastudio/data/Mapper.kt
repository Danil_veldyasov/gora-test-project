package com.example.android.project.testgorastudio.data

typealias Mapper<T, R> = (T) -> R