package com.example.android.project.testgorastudio.data.repository.users

import com.example.android.project.testgorastudio.data.repository.mappers.UserDTOtoUser
import com.example.android.project.testgorastudio.data.repository.users.datasource.UserNetworkDataSource
import com.example.android.project.testgorastudio.domen.model.User
import com.example.android.project.testgorastudio.domen.repository.UserRepository
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val userDao: UserDao,
    private val dataSource: UserNetworkDataSource
) : UserRepository {

    override suspend fun getUsers(): Flow<List<User>> {
        loadUsersWhenEmpty()
        return userDao.getUsers()
            .map {
                it.map {
                    UserDTOtoUser().invoke(it)
                }
            }
    }

    private suspend fun loadUsersWhenEmpty() {
        if (!userDao.hasUsers()) {
            userDao.save(dataSource.getUsers())
        }
    }
}

