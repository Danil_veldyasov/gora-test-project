package com.example.android.project.testgorastudio.ui.screens.photos

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android.project.testgorastudio.R
import com.example.android.project.testgorastudio.databinding.FragmentPhotosListBinding
import com.example.android.project.testgorastudio.databinding.FragmentUsersListBinding
import com.example.android.project.testgorastudio.ui.screens.photos.adapter.PhotoRecyclerAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class PhotosListFragment : Fragment(R.layout.fragment_photos_list) {
    private val photosViewModel: PhotosViewModel by viewModels()
    private lateinit var binding: FragmentPhotosListBinding
    private lateinit var adapter: PhotoRecyclerAdapter
    private var userId: Int? = null
    private val TAG = "PhotoListFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            userId = it.getInt("userId")
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentPhotosListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = PhotoRecyclerAdapter(requireContext())
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        photosViewModel.getPhotos(userId!!)
        lifecycleScope.launchWhenResumed {
            photosViewModel.uiState.collect { state ->
                render(state)
            }
        }
        binding.toolbar.findViewById<Button>(R.id.button_back).setOnClickListener {
            parentFragmentManager.popBackStack()
        }
        binding.recyclerView.adapter = adapter
    }

    private fun render(photosViewState: State){
        when(photosViewState){
            is State.Loading -> {
                Log.d(TAG, "Loading ")
                binding.progressBar.isVisible = true
                binding.recyclerView.isVisible = false
            }
            is State.Error -> {
                Log.d(TAG, "Error: ${photosViewState.message}")
                binding.progressBar.isVisible = false
                binding.recyclerView.isVisible = false
                Toast.makeText(requireContext(), "data retrieval error", Toast.LENGTH_SHORT).show()
            }
            is State.PhotosLoaded -> {
                binding.progressBar.isVisible = false
                binding.recyclerView.isVisible = true
                adapter.setPhotos(photosViewState.listPhotos)
                Log.d(TAG, "End: ${photosViewState.listPhotos.toString()} ")
            }

            else -> {
                Log.d(TAG, "WTF unknown state")
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(userId: Int) =
            PhotosListFragment().apply {
                arguments = Bundle().apply {
                    putInt("userId", userId)
                }
            }
    }
}