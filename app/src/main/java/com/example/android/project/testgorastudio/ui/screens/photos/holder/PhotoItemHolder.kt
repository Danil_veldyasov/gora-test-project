package com.example.android.project.testgorastudio.ui.screens.photos.holder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.android.project.testgorastudio.R

class PhotoItemHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var image: ImageView = itemView.findViewById(R.id.image_photo)
    var title: TextView = itemView.findViewById(R.id.title_photo)
}