package com.example.android.project.testgorastudio.data.repository.mappers

import com.example.android.project.testgorastudio.data.Mapper
import com.example.android.project.testgorastudio.data.model.PhotoDTO
import com.example.android.project.testgorastudio.domen.model.Photo

class PhotoDTOtoPhoto: Mapper<PhotoDTO, Photo> {
    override fun invoke(photoDTO: PhotoDTO): Photo {
        return Photo(
            photoDTO.albumId,
            photoDTO.id,
            photoDTO.title,
            photoDTO.url
        )
    }
}