package com.example.android.project.testgorastudio.domen.repository

import com.example.android.project.testgorastudio.domen.model.Photo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface PhotoRepository {
   suspend fun getPhotos(userId: Int): Flow<List<Photo>>
}