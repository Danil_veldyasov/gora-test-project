package com.example.android.project.testgorastudio.data.repository.photo

import com.example.android.project.testgorastudio.data.repository.mappers.PhotoDTOtoPhoto
import com.example.android.project.testgorastudio.data.repository.photo.datasource.PhotoNetworkDataSource
import com.example.android.project.testgorastudio.domen.model.Photo
import com.example.android.project.testgorastudio.domen.repository.PhotoRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PhotoRepositoryImpl @Inject constructor(
    private val photoDao: PhotoDao,
    private val dataSource: PhotoNetworkDataSource
): PhotoRepository {

    override suspend fun getPhotos(userId: Int): Flow<List<Photo>> {
        loadPhotosWhenEmpty(userId)
        return photoDao.getPhotos(userId)
            .map {
                it.map {
                    PhotoDTOtoPhoto().invoke(it)
                }
            }
    }

    private suspend fun loadPhotosWhenEmpty(userId: Int){
        if (!photoDao.hasPhotos(userId)){
            photoDao.save(dataSource.getPhotos(userId))
        }
    }
}