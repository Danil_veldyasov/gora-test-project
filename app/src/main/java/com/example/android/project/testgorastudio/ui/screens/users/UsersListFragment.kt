package com.example.android.project.testgorastudio.ui.screens.users

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android.project.testgorastudio.R
import com.example.android.project.testgorastudio.databinding.FragmentUsersListBinding
import com.example.android.project.testgorastudio.ui.screens.photos.PhotosListFragment
import com.example.android.project.testgorastudio.ui.screens.users.adapter.UserRecyclerAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.launch


@AndroidEntryPoint
class UsersListFragment : Fragment(R.layout.fragment_users_list) {
    private val usersViewModel: UsersViewModel by viewModels()
    private lateinit var binding: FragmentUsersListBinding
    private lateinit var adapter: UserRecyclerAdapter
    private val TAG = "UsersListFragment"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentUsersListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapter = UserRecyclerAdapter(object : UserRecyclerAdapter.OnUserClickListener{
            override fun onUserClick(userId: Int) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, PhotosListFragment.newInstance(userId))
                    .addToBackStack("photos")
                    .commit()
            }

        })
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                binding.recyclerView.context, (binding.recyclerView.layoutManager as LinearLayoutManager).orientation)
        )
        lifecycleScope.launchWhenResumed {
            usersViewModel.uiState.collect { state ->
                render(state)
            }
        }
    }

    private fun render(usersViewState: State){
        when(usersViewState){
            is State.Loading -> {
                Log.d(TAG, "Loading ")
                binding.progressBar.isVisible = true
                binding.recyclerView.isVisible = false
            }
            is State.Error -> {
                Log.d(TAG, "End: ${usersViewState.message} ")
                binding.progressBar.isVisible = false
                binding.recyclerView.isVisible = false
                Toast.makeText(requireContext(), "data retrieval error", Toast.LENGTH_SHORT).show()
            }
            is State.UsersLoaded -> {
                binding.progressBar.isVisible = false
                binding.recyclerView.isVisible = true
                adapter.setUsers(usersViewState.listUsers)
                Log.d(TAG, "End: ${usersViewState.listUsers.size} ")
            }

            else -> {
                Log.d(TAG, "WTF unknown state")
            }
        }
    }
}