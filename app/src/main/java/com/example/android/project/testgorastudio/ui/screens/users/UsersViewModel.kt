package com.example.android.project.testgorastudio.ui.screens.users

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.android.project.testgorastudio.domen.exception.FailureOrLoading
import com.example.android.project.testgorastudio.domen.interactors.GetUsers
import com.example.android.project.testgorastudio.domen.model.User
import com.example.android.project.testgorastudio.domen.usecase.UseCase
import dagger.assisted.AssistedInject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UsersViewModel @Inject constructor(
    private val getUsers: GetUsers,
    private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val initialState: State = State.Loading
    private val _uiState : MutableStateFlow<State> = MutableStateFlow(initialState)
    val uiState = _uiState.asStateFlow()

    init {
        viewModelScope.launch(Dispatchers.IO) {

            _uiState.emit(State.Loading)
            try {
                getUsers.run(UseCase.None()).collect {
                    _uiState.emit(State.UsersLoaded(it))
                }
            }
            catch (e: java.lang.Exception) {
                _uiState.emit(State.Error(e.message?: "Unknown error"))
            }

        }
    }


}