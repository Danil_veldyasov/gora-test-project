package com.example.android.project.testgorastudio.data.repository.mappers

import com.example.android.project.testgorastudio.data.Mapper
import com.example.android.project.testgorastudio.data.model.UserDTO
import com.example.android.project.testgorastudio.domen.model.User

class UserDTOtoUser: Mapper<UserDTO, User> {
    override fun invoke(userDTO: UserDTO): User {
        return User(
            userDTO.id,
            userDTO.name,
            userDTO.username
        )
    }
}