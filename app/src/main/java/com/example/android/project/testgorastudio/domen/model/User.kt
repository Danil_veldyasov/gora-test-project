package com.example.android.project.testgorastudio.domen.model

data class User(
    val id: Int,
    val name: String,
    val username: String
)


