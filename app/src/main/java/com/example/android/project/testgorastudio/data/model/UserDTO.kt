package com.example.android.project.testgorastudio.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class UserDTO(
    @PrimaryKey
    val id: Int,
    val name: String,
    val username: String
)


