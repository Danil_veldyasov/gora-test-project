package com.example.android.project.testgorastudio.ui.screens.photos

import com.example.android.project.testgorastudio.domen.model.Photo

sealed class State(){
    object Loading: State()
    class Error(val message: String): State()
    class PhotosLoaded(val listPhotos: List<Photo>): State()
}
