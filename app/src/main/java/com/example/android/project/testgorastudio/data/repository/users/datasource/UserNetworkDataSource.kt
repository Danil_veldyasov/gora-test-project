package com.example.android.project.testgorastudio.data.repository.users.datasource

import com.example.android.project.testgorastudio.data.model.UserDTO

interface UserNetworkDataSource {
    suspend fun getUsers(): List<UserDTO>
}