package com.example.android.project.testgorastudio.data.repository.users

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.android.project.testgorastudio.data.model.UserDTO
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {

    @Insert(onConflict = REPLACE)
    fun save(userDTO: List<UserDTO>)

    @Query("SELECT * FROM users")
    fun getUsers(): Flow<List<UserDTO>>

    @Query("SELECT count(*)>0  FROM users")
    fun hasUsers(): Boolean
}