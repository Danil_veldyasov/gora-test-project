package com.example.android.project.testgorastudio.data.repository.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.android.project.testgorastudio.data.model.PhotoDTO
import com.example.android.project.testgorastudio.data.model.UserDTO
import com.example.android.project.testgorastudio.data.repository.photo.PhotoDao
import com.example.android.project.testgorastudio.data.repository.users.UserDao

@Database(entities = [UserDTO::class, PhotoDTO::class],version = 1, exportSchema = false)
abstract class AppDataBase: RoomDatabase(){
    abstract fun getUserDao(): UserDao
    abstract fun getPhotoDao(): PhotoDao
}