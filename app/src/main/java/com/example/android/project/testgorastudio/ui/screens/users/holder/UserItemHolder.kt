package com.example.android.project.testgorastudio.ui.screens.users.holder

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.android.project.testgorastudio.R

    class UserItemHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var userName: TextView = itemView.findViewById(R.id.user_name)
}