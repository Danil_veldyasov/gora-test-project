package com.example.android.project.testgorastudio.data.repository.photo

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.android.project.testgorastudio.data.model.PhotoDTO
import com.example.android.project.testgorastudio.data.model.UserDTO
import kotlinx.coroutines.flow.Flow

@Dao
interface PhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(photoDTO: List<PhotoDTO>)

    @Query("SELECT * FROM photos WHERE albumId = :userId")
    fun getPhotos(userId: Int): Flow<List<PhotoDTO>>

    @Query("SELECT count(*)>0  FROM photos WHERE albumId = :userId")
    fun hasPhotos(userId: Int): Boolean
}